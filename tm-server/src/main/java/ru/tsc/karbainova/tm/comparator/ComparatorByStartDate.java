package ru.tsc.karbainova.tm.comparator;

import ru.tsc.karbainova.tm.api.entity.IHasStartDate;

import java.util.Comparator;

public class ComparatorByStartDate implements Comparator<IHasStartDate> {

    private static final ComparatorByStartDate instance = new ComparatorByStartDate();

    private ComparatorByStartDate() {
    }

    public static ComparatorByStartDate getInstance() {
        return instance;
    }

    @Override
    public int compare(IHasStartDate o1, IHasStartDate o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }
}
