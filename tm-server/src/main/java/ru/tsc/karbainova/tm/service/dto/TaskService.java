package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.dto.ITaskService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.dto.TaskDTO;
import ru.tsc.karbainova.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractOwnerService<TaskDTO> implements ITaskService {

    public TaskService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> findAllTaskByUserId(String userId) {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<TaskDTO> collection) {
        if (collection == null) return;
        for (TaskDTO i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO add(TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            @NonNull final ITaskDTORepository taskDTORepository = new TaskDTORepository(entityManager);
            taskDTORepository.add(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NonNull final ITaskDTORepository taskDTORepository = new TaskDTORepository(entityManager);
            taskDTORepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeByIdUserId(userId, task.getId());
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO updateById(@NonNull String userId, @NonNull String id,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);

            TaskDTO task = taskRepository.findByIdUserId(userId, id);
            if (task == null) throw new ProjectNotFoundException();
            task.setName(name);
            task.setDescription(description);
            final ITaskDTORepository repository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO findByIdUserId(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }
}
