package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;

import javax.persistence.EntityManager;
import java.sql.Connection;

public interface IConnectionService {

    @NonNull EntityManager getEntityManager();
}
